.. test3 documentation master file, created by
   sphinx-quickstart on Wed Jul  8 15:28:19 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test3's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   PyCharm
   Sphinx
   git



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
